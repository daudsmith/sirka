import React from 'react';
import { Row, Card, CardBody, Col, Input, Button } from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'next/router';
import SiteLogoMolecule from '../molecules/siteLogoMolecule';
import SideNavMolecule from '../molecules/sideNavMolecule';
import Swal from 'sweetalert2'
class NavigationOrganism extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
     this.addProfile = this.addProfile.bind(this);
     this.searchProfile = this.searchProfile.bind(this);
   }

   searchProfile = (e) => {
      console.log(e)
         Swal.fire({
            title: 'Ooopss',
            text: 'Daud Cant Find API for Search',
            icon: 'warning',
            confirmButtonText: 'Ok'
          })
   }

   addProfile() {
      this.props.router.push({
         pathname: '/AddProfile'
      })
   }

   render() {

      return (
         <>
            <Card className='card-navigation'>
                <CardBody>
                    <Row>
                        <SiteLogoMolecule/>
                        <SideNavMolecule/>
                    </Row>
                </CardBody>
            </Card>
            <style jsx global>
               {`
                  .logo-image {
                     width: 40px;
                     height: 40px;
                     display: block;
                     margin-left: auto;
                     margin-right: auto;
                  }
                  .card-navigation {
                     display:flex;
                     box-shadow: 3px 2px 10px rgba(189, 195, 199,1.0); 
                     padding: 2px;
                     margin-bottom: 6px;
                  }
               `}
            </style>
         </>
      );
   }
}

const mapStateToProps = (state) => {
   return {
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(NavigationOrganism))