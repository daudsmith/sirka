import React from 'react';
import { Col, Input, Button } from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'next/router';
import Swal from 'sweetalert2'

class SideNavMolecule extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
     this.addProfile = this.addProfile.bind(this);
     this.searchProfile = this.searchProfile.bind(this);
   }

   searchProfile = (e) => {
      console.log(e)
         Swal.fire({
            title: 'Ooopss',
            text: 'Daud Cant Find API for Search',
            icon: 'warning',
            confirmButtonText: 'Ok'
          })
   }

   addProfile() {
      this.props.router.push({
         pathname: '/AddProfile'
      })
   }

   render() {

      return (
         <>
            <Col xs={6}>
            </Col>
            <Col xs={2}>
                <Input className='object-category' placeholder='Serach : Ex John'/>
            </Col>
            <Col xs={1}>
                <div className='content-category'>
                <Button
                    outline block
                    onClick={() => this.searchProfile()}
                    className='object-category'
                    color="primary">Search</Button>
                </div>
            </Col>
            <Col xs={1}>
                <div className='content-category'>
                <Button
                    outline block
                    className='object-category'
                    onClick={this.addProfile}
                    color="primary">Add</Button>
                </div>
            </Col>
         </>
      );
   }
}

const mapStateToProps = (state) => {
   return {
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SideNavMolecule))