import React from 'react';
import { Col } from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'next/router';
import { FcBiomass } from "react-icons/fc";
class SiteLogoMolecule extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
   }

   render() {

      return (
         <>
            <Col xs={2}>
                <FcBiomass className='logo-image'/>
            </Col>
         </>
      );
   }
}

const mapStateToProps = (state) => {
   return {
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(SiteLogoMolecule))