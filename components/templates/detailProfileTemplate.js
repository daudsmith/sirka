import React from 'react';
import { Container, Row, Card, CardBody, Col } from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'next/router';
import { requestUser } from '../../redux/actions/users'
import NavigationOrganism from '../organisms/navigationOrganism'
import { FcNightPortrait } from "react-icons/fc";

class DetailProfileTemplate extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
   }

   componentDidMount() {
      this.props.requestUser();
   }

   render() {
      let detail = this.props.users.profile.data;

      return (
         <div className="d-flex justify-content-center p-5">
            <Container>
               <NavigationOrganism />
               <Col xs='12'>
                  <Card className='card-content'>
                     <CardBody>
                           <Row>
                              <Col xs='2'>
                                 <Card className='card-content'>
                                    <CardBody>
                                       <FcNightPortrait className='user-image' />
                                    </CardBody>
                                 </Card>
                              </Col>
                              <Col xs='10'>
                                 <Card className='card-content'>
                                    <CardBody>
                                       <Row className='text-content'>
                                          <Col xs='3'>
                                             <strong>First Name :</strong> {detail.name.firstname}
                                          </Col>
                                          <Col xs='3'>
                                          <strong>Last Name :</strong>  {detail.name.lastname}
                                          </Col>
                                          <Col xs='6'>
                                          </Col>
                                       </Row>
                                       <Row className='text-content'>
                                          <Col xs='3'>
                                          <strong>User Name :</strong>  {detail.username}
                                          </Col>
                                          <Col xs='3'>
                                          <strong>Password :</strong>  {detail.password}
                                          </Col>
                                          <Col xs='3'>
                                          <strong>Email :</strong>  {detail.email}
                                          </Col>
                                          <Col xs='3'>
                                          <strong>Phone :</strong>  {detail.phone}
                                          </Col>
                                       </Row>
                                       <Row className='text-content'>
                                          <Col xs='12'>
                                          <strong>Address :</strong>  {detail.address.street}, {detail.address.number}, {detail.address.city}, {detail.address.zipcode}
                                          </Col>
                                       </Row>
                                    </CardBody>
                                 </Card>
                              </Col>
                           </Row>
                     </CardBody>
                  </Card>
               </Col>
            </Container>
         </div>
      );
   }
}

const mapStateToProps = (state) => {
   return {
      users: state.users
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestUser: (id) => dispatch(requestUser(id)),
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(DetailProfileTemplate))