import React from 'react';
import { Container, Row, Card, CardBody, Col, Form, FormGroup, Input, Button } from 'reactstrap'
import { connect } from 'react-redux'
import { withRouter } from 'next/router';
import { requestUser } from '../../redux/actions/users'
import NavigationOrganism from '../organisms/navigationOrganism'
import { ADD_USER } from '../../api';
import httpRequest from '../../api/services';
import { FcGoodDecision } from "react-icons/fc";
import Swal from 'sweetalert2'

class AddProfileTemplate extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
         email:'',
         username:'',
         password:'',
         name:{
            firstname:'',
            lastname:''
         },
         address:{
            city:'',
            street:'',
            number:'',
            zipcode:'',
            geolocation:{
                  lat:'',
                  long:''
            }
         },
         phone:''
     };
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);

   }

   handleChange(e) {
      this.setState({
          [e.target.name]: e.target.value
      })
  }

  handleSubmit(e) {
      e.preventDefault()
      let self = this;
      let newParams = {
         "email": this.state.email,
         "username": this.state.username,
         "password": this.state.password,
         name:{
            "firstname": this.state.firstname,
            "lastname": this.state.lastname
         },
         address:{
            "city": this.state.city,
            "street": this.state.street,
            "number": this.state.number,
            "zipcode": this.state.zipcode,
            geolocation:{
                  "lat": this.state.lat,
                  "long": this.state.long
            }
         },
         "phone": this.state.phone
      };
      httpRequest({
         method: 'POST',
         url: ADD_USER,
         data: newParams
      }).then(function(response) {
         console.log("User Add",response)
         Swal.fire({
            title: 'Success',
            text: 'Check Your Console',
            icon: 'success',
            confirmButtonText: 'Ok'
          })
         self.props.router.push({
            pathname: '/'
         });
      }).catch(error =>{
         console.log(error)
      });
  }

   render() {

      return (
         <div className="d-flex justify-content-center p-5">
            <Container>
            <NavigationOrganism />
               <Card className='card-content'>
                  <CardBody>
                     <Col xs='12'>
                        <Card className='card-form'>
                           <CardBody>
                                 <FcGoodDecision className='add-user-image' />
                                 <Row>
                                    <Form>
                                       <Col xs='12'>
                                          <FormGroup>
                                             <Input 
                                                type="text"
                                                name="firstname"
                                                placeholder='First Name'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="text"
                                                name="lastname"
                                                placeholder='Last Name'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                          <FormGroup>
                                             <Input 
                                                type="text"
                                                name="username"
                                                placeholder='username'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="email"
                                                name="email"
                                                placeholder='email'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="number"
                                                name="phone"
                                                placeholder='phone'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="password"
                                                name="password"
                                                placeholder='password'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                          <FormGroup>
                                             <Input 
                                                type="text"
                                                name="street"
                                                placeholder='Street'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="number"
                                                name="number"
                                                placeholder='Number'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="text"
                                                name="city"
                                                placeholder='City'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="text"
                                                name="zipcode"
                                                placeholder='Zip Code'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="text"
                                                name="lat"
                                                placeholder='Lat'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Col xs='12'>
                                       <FormGroup>
                                             <Input 
                                                type="text"
                                                name="long"
                                                placeholder='Long'
                                                onChange={(e) => this.handleChange(e)}/>
                                          </FormGroup>
                                       </Col>
                                       <Row>
                                          <FormGroup>
                                             <Button color="primary" onClick={this.handleSubmit} block>Add New User</Button>
                                          </FormGroup>
                                       </Row>
                                    </Form>
                                 </Row>
                           </CardBody>
                        </Card>
                     </Col>
                  </CardBody>
               </Card>
            </Container>
         </div>
      );
   }
}

const mapStateToProps = (state) => {
   return {
      users: state.users
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestUser: (id) => dispatch(requestUser(id)),
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AddProfileTemplate))