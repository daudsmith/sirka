import React from 'react';
import { Container, Row, Card, CardBody, Col, NavLink, Table, Button } from 'reactstrap'
import { connect } from 'react-redux'
import { DELETE_USER, EDIT_USER } from '../../api';
import httpRequest from '../../api/services';
import { withRouter } from 'next/router';
import Swal from 'sweetalert2'
import { requestAllUser, requestUser } from '../../redux/actions/users'
import NavigationOrganism from '../organisms/navigationOrganism'
import { FcDatabase, FcDataConfiguration, FcDeleteDatabase } from "react-icons/fc";
class ProfileTemplate extends React.Component {
   constructor(props) {
      super(props);
      this.state = {
     };
     this.selectProfile = this.selectProfile.bind(this);
     this.selectProfileDelete = this.selectProfileDelete.bind(this);
     this.selectProfileEdit = this.selectProfileEdit.bind(this);
   }

   componentDidMount() {
      this.props.requestAllUser();
      this.props.requestUser();
   }

   selectProfile = (users) => {
      let self = this;
      let newParams =  users.id
      self.props.requestUser(newParams).then(function(response) {
      if (response) {
         self.props.router.push({
            pathname: '/ProfileDetail'
         });
      }
      }).catch(error =>{
         console.log('error', error)
      });
   }

   selectProfileEdit = (users) => {
      let self = this;
      let newParams =  users.id
      httpRequest.put(EDIT_USER+'/'+newParams).then(function(response) {
      console.log("Delete User",response)
      if (response) {
         Swal.fire({
            title: 'Success',
            text: 'Check Your Console',
            icon: 'success',
            confirmButtonText: 'Ok'
          })
         self.props.router.push({
            pathname: '/'
         });
      }
      }).catch(error =>{
         console.log('error', error)
      });
   }

   selectProfileDelete = (users) => {
      let self = this;
      let newParams =  users.id
      httpRequest.delete(DELETE_USER+'/'+newParams).then(function(response) {
      console.log("Delete User",response)
      if (response) {
         Swal.fire({
            title: 'Success',
            text: 'Check Your Console',
            icon: 'success',
            confirmButtonText: 'Ok'
          })
         self.props.router.push({
            pathname: '/'
         });
      }
      }).catch(error =>{
         console.log('error', error)
      });
   }
   

   render() {
      let data = this.props.users.users;
      

      return (
         <div className="d-flex justify-content-center p-5">
            <Container>
                  <NavigationOrganism />
               <Row>
                  <Col xs='12'>
                     <Card className='card-content'>
                        <CardBody>
                           <Table hover responsive>
                              <thead>
                                 <tr>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Username</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                              { data.data && data.data.map ((users, i) => {
                                 return <tr key={i} >
                                          <td>
                                             {users.name.firstname}
                                          </td>
                                          <td>
                                             {users.name.lastname}
                                          </td>
                                          <td>
                                             {users.email}
                                          </td>
                                          <td>
                                             <Row>
                                                <Col sm='12'>
                                                   <Button className='button-action' color='primary' outline onClick={() => this.selectProfile(users)}>
                                                      <FcDatabase/>
                                                   </Button>
                                                   <Button className='button-action' color='primary' outline onClick={() => this.selectProfileEdit(users)}>
                                                      <FcDataConfiguration/>
                                                   </Button>
                                                   <Button className='button-action' color='primary' outline onClick={() => this.selectProfileDelete(users)} >
                                                      <FcDeleteDatabase/>
                                                   </Button>
                                                </Col>
                                             </Row>
                                          </td>
                                       </tr>
                                 }
                              )}
                              </tbody>
                           </Table>
                        </CardBody>
                     </Card>
                  </Col>
               </Row>
            </Container>
         </div>
      );
   }
}

const mapStateToProps = (state) => {
   return {
      users: state.users
   }
}

const mapDispatchToProps = (dispatch) => {
   return {
      requestAllUser: () => dispatch(requestAllUser()),
      requestUser: (id) => dispatch(requestUser(id)),
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProfileTemplate))