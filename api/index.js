export const GET_ALL_USER = `https://fakestoreapi.com/users`;
export const GET_USER = `https://fakestoreapi.com/users`;
export const ADD_USER = `https://fakestoreapi.com/users`;
export const DELETE_USER = `https://fakestoreapi.com/users`;
export const EDIT_USER = `https://fakestoreapi.com/users`;