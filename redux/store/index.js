import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import rootReducers from '../reducers'
import { persistStore, persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const makeConfiguredStore = (reducer, initialState) => {
  return createStore(
    reducer,
    initialState,
    applyMiddleware(thunkMiddleware)
  )
}

const initialState = {};

export default function Main (initialState, {isServer}) {

		if (isServer) {
			initialState = initialState;
			return makeConfiguredStore(rootReducers, initialState);
		} else {
			let persistConfig = {
				key: window.location.host,
				storage,
				whitelist: ['users']
			};
  
  

			let persistedReducer = persistReducer(persistConfig, rootReducers);
			let store = makeConfiguredStore(persistedReducer, initialState);

			store.__persistor = persistStore(store);
			return store;
		}
}
