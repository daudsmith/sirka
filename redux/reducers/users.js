import { FETCH_ALL_USER_BEGIN, FETCH_ALL_USER_SUCCESS, FETCH_ALL_USER_FAILURE } from '../actions/users'

const initialUser = {
	isFetching: false,
	isError: false,
	users: {
		data: null,
  		isFetching: false, 
  		error: false
	},
	profile: {
		data: null,
		isFetching: false,
		isError: false
	}
}

const usersReducer = ( state = initialUser , action ) => {
  	switch( action.type ){
  		
		case 'FETCH_ALL_USER_BEGIN':
				state = {...state, 
					users: {
						...state.users,
						isFetching: true, 
						error: false
				   }
				}
				break;
		case 'FETCH_ALL_USER_SUCCESS':
				state = { ...state, 
					  users: {
						  ...state.users,
						  data: action.payload,
						  isFetching: false, 
						  error: false
					 }
				  }
				break;
		case 'FETCH_ALL_USER_FAILURE':
				state = {...state, 
					users: {
						...state.users,
						data: action.payload,
						isFetching: false, 
						error: true
				   }
				}
				break;
		case 'FETCH_USER_BEGIN':
				state = {...state, 
					profile: {
						...state.profile,
						isFetching: true, 
						error: false
					}
				}
				break;
		case 'FETCH_USER_SUCCESS':
				state = { ...state, 
						profile: {
							...state.profile,
							data: action.payload,
							isFetching: false, 
							error: false
						}
					}
				break;
		case 'FETCH_USER_FAILURE':
				state = {...state, 
					profile: {
						...state.profile,
						data: action.payload,
						isFetching: false, 
						error: true
					}
				}
				break;
	    default:
	      	break;
	}
  	return state
}

export default usersReducer;