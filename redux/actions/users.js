import { GET_ALL_USER, GET_USER } from '../../api'
import httpRequest from '../../api/services'

export const FETCH_ALL_USER_BEGIN = 'FETCH_ALL_USER_BEGIN';
export const FETCH_ALL_USER_SUCCESS = 'FETCH_ALL_USER_SUCCESS';
export const FETCH_ALL_USER_FAILURE = 'FETCH_ALL_USER_FAILURE';

export const FETCH_USER_BEGIN = 'FETCH_USER_BEGIN';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';


export const fetchAllUserBegin = () => ({
	type: 'FETCH_ALL_USER_BEGIN'
});

export const fetchAllUserSuccess = data => ({
	type: 'FETCH_ALL_USER_SUCCESS',
	payload: data
});

export const fetchAllUserFailure = error => ({
	type: 'FETCH_ALL_USER_FAILURE',
	payload: error
});

export const fetchUserBegin = () => ({
	type: 'FETCH_USER_BEGIN'
});

export const fetchUserSuccess = data => ({
	type: 'FETCH_USER_SUCCESS',
	payload: data
});

export const fetchUserFailure = error => ({
	type: 'FETCH_USER_FAILURE',
	payload: error
});

export const requestAllUser = () => {
	return dispatch => {
		dispatch(fetchAllUserBegin())
		
	  	return httpRequest.get(GET_ALL_USER).then(function (response) {
	     	if (response.data.code == 200) {
	     		dispatch(fetchAllUserSuccess(response.data))
		      	return response.data;
	     	} else {
	     		dispatch(fetchAllUserFailure(response.data))
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	  		
	     	dispatch(fetchAllUserFailure(error.response))
	  	});
	}
}

export const requestUser = (id) => {
	return dispatch => {
		dispatch(fetchUserBegin())
		
	  	return httpRequest.get(GET_USER+'/'+id).then(function (response) {
	     	if (response.data.code == 200) {
	     		dispatch(fetchUserSuccess(response.data))
		      	return response.data;
	     	} else {
	     		dispatch(fetchUserFailure(response.data))
		      	return response.data;
	     	}
	  	}).catch(function (error) {
	  		
	     	dispatch(fetchUserFailure(error.response))
	  	});
	}
}