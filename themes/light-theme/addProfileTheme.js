import React, {Component} from 'react';
import PageAddProfile from '../../components/pages/pageAddProfile'

class AddProfileTheme extends Component {

    render () {
          return (
            <div>
                <PageAddProfile/>
                <style jsx global>
               {`
                  .add-user-image {
                     width: 60px;
                     height: 60px;
                     display: block;
                     margin-left: auto;
                     margin-right: auto;
                  }
                  .card-navigation{
                     display:flex;
                     box-shadow: 3px 2px 10px rgba(189, 195, 199,1.0); 
                     padding: 2px;
                     margin-bottom: 6px;
                  }
                  .card-content {
                     box-shadow: 2px 2px 7px rgba(189, 195, 199,1.0);
                     justify-content: center;
                  }
                  .card-form {
                     box-shadow: 1px 1px 10px rgba(189, 195, 199,1.0);
                     justify-content: center;
                     width: 30%;
                     display: block;
                     margin-left: auto;
                     margin-right: auto;
                  }
                  .text-content {
                     text-align: left;
                     font-size: 14px;
                     margin-bottom: 20px;
                  }
                  .user-image {
                     width: 122px;
                     height: 122px;
                     display: block;
                     margin-left: auto;
                     margin-right: auto;
                  }
               `}
            </style>
            </div>
          )
    }
  }
  
  export default AddProfileTheme
