import React, {Component} from 'react';
import PageProfile from '../../components/pages/pageProfile'

class index extends Component {

    render () {
          return (
            <div>
                <PageProfile/>
                <style jsx global>
               {`
                  .button-action {
                     size: 15px;
                     margin-right: 2px;
                  }
                  .card-content {
                     box-shadow: 2px 2px 7px rgba(189, 195, 199,1.0); 
                     text-align: center;
                     font-size: 13px;
                     justify-content: center;
                  }
                  .content-category {
                     margin-bottom: 5px;
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                     justify-content: center;
                  }
                  .object-category {
                     text-align: center;
                     font-size: 12px;
                     font-weight: bold;
                  }
                  .logo {
                     width: 35px;
                     height: 35px;
                     justify-content: center;
                     margin: 0 auto;
                     padding: 0 auto;
                   }
               `}
            </style>
            </div>
          )
    }
  }
  
  export default index
