import React, {Component} from 'react';
import PageDetail from '../../components/pages/pageDetail'

class ProfileDetailTheme extends Component {

    render () {
          return (
            <div>
                <PageDetail/>
              <style jsx global>
               {`
                  .card-navigation{
                     display:flex;
                     box-shadow: 3px 2px 10px rgba(189, 195, 199,1.0); 
                     padding: 2px;
                     margin-bottom: 6px;
                  }
                  .card-content {
                     box-shadow: 2px 2px 7px rgba(189, 195, 199,1.0);
                     justify-content: center;
                  }
                  .text-content {
                     text-align: left;
                     font-size: 14px;
                     margin-bottom: 20px;
                  }
                  .user-image {
                     width: 122px;
                     height: 122px;
                     display: block;
                     margin-left: auto;
                     margin-right: auto;
                  }
               `}
            </style>
            </div>
          )
    }
  }
  
  export default ProfileDetailTheme
